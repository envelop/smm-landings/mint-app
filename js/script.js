
var swiperIntro = new Swiper('.swiper-intro', {
    // Optional parameters
    loop: true,
    effect: "fade",
    spaceBetween: 0,
    crossFade: true,
    autoplay: true,
    delay: 1500,
    speed: 1000,
    disableOnInteraction: false
});

var swiperMedia = new Swiper('.swiper-media', {
    // Optional parameters
    loop: false,
    spaceBetween: 0,
    crossFade: true,
    autoplay: false,
    speed: 500,
    disableOnInteraction: false,
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
      },
      navigation: {
        nextEl: '.swiper-media-button-next',
        prevEl: '.swiper-media-button-prev',
      },
});


// Use Case Section

var swiperUse = new Swiper('.swiper-use', {
    loop: false,
    spaceBetween: 0,
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    autoplay: false,
    speed: 500,
    disableOnInteraction: false,
    pagination: false,
    autoHeight: true,
    allowTouchMove: false
});

var btnUse = document.querySelectorAll('.btn-use');


btnUse.forEach(function(btn) {
	btn.addEventListener("click", toggleActive);
});

function toggleActive () {
    var curBtn = this;
    var curSlide = curBtn.dataset.slideNum;
    
    if (!curBtn.matches('.active')) {

        for (var i = 0; i < btnUse.length; ++i) {
            btnUse[i].classList.remove('active');
         }

        curBtn.classList.add('active');

        swiperUse.slideTo(curSlide);
    }
}


// faq accordion
var accordion = new Accordion('.accordion-container',{
    hideAll: false,
    showAll: false,
    showFirst: true,
    panelId: 0
});

// toggle mobile menu
var mobMenuToggle = document.getElementsByClassName('header__menu-toggle')[0];
var scrollLinks = document.getElementsByClassName('scroll-link');
var numScrollLinks = scrollLinks.length;

mobMenuToggle.addEventListener('click', function () {
    toggleMobileMenu(false);
});

function toggleMobileMenu(close) {
    if(!close) {
        mobMenuToggle.classList.toggle("open");
    } else {
        mobMenuToggle.classList.remove("open");
    }
}


// all anchor links
var lnks = document .querySelectorAll('.scroll-link');

for (var i = 0; i < lnks.length; i++) {
    lnks[i].onclick = function(e){
        e.preventDefault();
        var curLnk = this;

        var curLnkHash = curLnk.getAttribute("href").substring(1);
        console.log(curLnkHash);
        var targSec = document.getElementById(curLnkHash);

        if(!isHidden(mobMenuToggle)) {
            toggleMobileMenu(true);
        }

        window.scrollTo({
            top: targSec.offsetTop,
            behavior: "smooth"
        });

    };
}


// check if element is visible - use to detect mobile view
function isHidden(el) {
  return (el.offsetParent === null)
}

// COOKIE ALERT

// COOKIE TOOLS
// END COOKIE TOOLS
function checkCookieAlert() {
    const isAgreed = localStorage.getItem('cookie_agreed');
    if ( !isAgreed ) {
        document.querySelector('.sec-cookie').style.display = null;
    }
}
function hideCookieAlert() {
    document.querySelector('.sec-cookie').style.display = 'none';
    localStorage.setItem('cookie_agreed', true);
}
document.querySelector('#cookie_agree').onclick = hideCookieAlert;
checkCookieAlert();
// END COOKIE ALERT