# Main App Layout



## We use gulp.js

To start the project:
go to the project's folder 
and run

```
npm install
gulp

```


### Dev & Build with docker
https://envelop.gitlab.io/design-and-layout/main-app-wrapper/
```bash
# Local run
docker run -it --rm  -v $PWD:/app node:16 /bin/bash -c 'cd /app && npm install && npx gulp'
##or

docker run -it --rm  -v $PWD:/app node:16 /bin/bash -c 'cd /app && npm install && npm run dev'

#production build
docker run -it --rm  -v $PWD:/app node:16 /bin/bash -c 'cd /app && npm install && npm run build'
```